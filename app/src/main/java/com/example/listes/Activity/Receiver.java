package com.example.listes.Activity;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.example.listes.Database.Database;
import com.example.listes.Items.TodoItem;
import com.example.listes.R;

import static com.example.listes.Activity.MainActivity.CHANNEL_ID;

public class Receiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();

        if (extras != null) {
            TodoItem item = Database.getItem(extras.getString("nom"));

            if(item !=null && !item.isDone()) {
                String titre = extras.getString("nom")+" : echéance passée !";
                String message = extras.getString("date")+" : tâche toujours pas effectuée !";

                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle(titre)
                        .setContentText(message)
                        .setOngoing(false)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setAutoCancel(true);
                manager.notify(12345, builder.build());
            }
        }
    }
}
