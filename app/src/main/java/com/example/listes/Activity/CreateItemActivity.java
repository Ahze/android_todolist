package com.example.listes.Activity;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.listes.Database.Database;
import com.example.listes.Items.TodoItem;
import com.example.listes.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.support.v4.content.ContextCompat.getSystemService;

public class CreateItemActivity extends AppCompatActivity {

    private Button boutonValider;
    private EditText itemField;
    private RadioGroup tagRadio;
    private TodoItem item;
    private TextView echeanceView   ;
    private long dateEcheance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_item);

        itemField = findViewById(R.id.labelItem);
        tagRadio = findViewById(R.id.tagRadio);
        echeanceView = findViewById(R.id.viewEcheance);


        Bundle extras = getIntent().getExtras();
        //Si extras != null, alors on MODIFIE l item
        if (extras != null) {
            item = (TodoItem)extras.get("item");
            itemField.setText(item.getLabel());
            tagRadio.check(item.getTag().getId());
            dateEcheance = item.getEcheance();
        }
        else{
            dateEcheance = new Date().getTime();
        }

        updateDate();

        Button b = findViewById(R.id.boutonDate);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dpd = new DatePickerDialog(view.getContext());
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR,year);
                        c.set(Calendar.MONTH,month);
                        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                        dateEcheance = c.getTimeInMillis();
                        updateDate();
                    }
                });
                dpd.show();
            }
        });


        boutonValider = findViewById(R.id.validerBouton);
        boutonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String nomItem = itemField.getText().toString();
                        TodoItem.Tags tagItem;
                        if(((RadioButton)findViewById(R.id.faibleBouton)).isChecked()){
                            tagItem = TodoItem.Tags.Faible;
                        }
                        else if(((RadioButton)findViewById(R.id.moyenBouton)).isChecked()){
                            tagItem = TodoItem.Tags.Normal;
                        }
                        else{
                            tagItem = TodoItem.Tags.Important;
                        }
                        if( dateEcheance != -1) {
                            if(item!=null) {
                                item.setLabel(nomItem);
                                item.setTag(tagItem);
                                Database.getDb().todoItemDao().updateItem(item);
                            }
                            else {
                                item = new TodoItem(nomItem, tagItem, dateEcheance);
                                Database.getDb().todoItemDao().insertAll(item);
                            }
                        }
                        AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(CreateItemActivity.this, Receiver.class);
                        intent.putExtra("nom",nomItem);
                        intent.putExtra("date",new SimpleDateFormat("dd-MM-yy").format(new Date(dateEcheance)));
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(CreateItemActivity.this, 0, intent, 0);
                        am.set(AlarmManager.RTC_WAKEUP, dateEcheance, pendingIntent);
                        finish();
                    }
                }).start();
            }
        });


    }

    public void updateDate(){
        echeanceView.setText(new SimpleDateFormat("dd-MM-yy").format(new Date(dateEcheance)));
    }

}
