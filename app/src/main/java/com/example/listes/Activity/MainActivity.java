package com.example.listes.Activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.listes.Database.Database;
import com.example.listes.Items.TodoItem;
import com.example.listes.Items.TodoItemTouchHelperCallback;
import com.example.listes.R;
import com.example.listes.Items.RecyclerAdapter;
import com.example.listes.Database.AndroidDatabaseManager;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recycler;
    RecyclerAdapter adapter;
    public static final String CHANNEL_ID = "88";

    FloatingActionButton fabMenu, fabAjouter, fabToutSupprimer, fabDebugger;
    boolean isFABOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.createNotificationChannel();
        setContentView(R.layout.activity_main);
        Database.init(getApplicationContext());

        fabMenu = findViewById(R.id.fabMenu);
        fabAjouter = findViewById(R.id.fabAjouterItem);
        fabToutSupprimer = findViewById(R.id.fabToutSupprimer);
        fabDebugger = findViewById(R.id.fabDebbuger);

        fabMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });


        fabAjouter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this , CreateItemActivity.class) ;
                startActivity(i);
            }
        });


        fabToutSupprimer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Database.truncateTable();
                adapter.refresh();
            }
        });

        fabDebugger.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AndroidDatabaseManager.class);
                startActivity(i);
            }
        });


        recycler = findViewById (R.id.recycler) ;
        LinearLayoutManager manager = new LinearLayoutManager( this ) ;
        recycler.setLayoutManager(manager) ;

        adapter = new RecyclerAdapter(Database.loadDataSet(), Typeface.createFromAsset(getAssets(), "fonts/minecraft.ttf"));
        recycler.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new TodoItemTouchHelperCallback(adapter, this);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recycler);

        this.popNotification();


    }



    @Override
    public void onResume(){
        super.onResume();
        adapter.refresh();
    }

    private void showFABMenu(){
        isFABOpen=true;
        fabAjouter.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fabToutSupprimer.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        fabDebugger.animate().translationY(-getResources().getDimension(R.dimen.standard_155));
    }

    private void closeFABMenu(){
        isFABOpen=false;
        fabAjouter.animate().translationY(0);
        fabToutSupprimer.animate().translationY(0);
        fabDebugger.animate().translationY(0);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "ToDoItem", importance);

            channel.setDescription("ToDoListes");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    private void popNotification() {
        List<TodoItem> list = Database.loadDataSet();
        long count = list.stream().filter(item -> item.getTag().equals(TodoItem.Tags.Important) && !item.isDone()).count();
        if(count>3) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle("Attention !")
                    .setContentText("Vous avez "+count+" tâches importantes non effectuées !")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            notificationManager.notify(1, builder.build());
        }
    }



}
