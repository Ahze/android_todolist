package com.example.listes.Database;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.listes.Items.TodoItem;

import java.util.ArrayList;
import java.util.List;

public class Database {

    static private AppDatabase db;

    public static void init(Context context){
        db = Room.databaseBuilder(context,
                AppDatabase.class, "item-database").fallbackToDestructiveMigration().build();
    }

    public static AppDatabase getDb(){
        return db;
    }

    public static List<TodoItem> loadDataSet(){
        final List<TodoItem> res = new ArrayList<>();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for(TodoItem item : Database.getDb().todoItemDao().getAll())
                    res.add(item);
            }
        });
        t.start();
        try{
            t.join();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void truncateTable(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Database.getDb().todoItemDao().deleteAll();
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static TodoItem res;
    public static TodoItem getItem(final int id){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                 res = Database.getDb().todoItemDao().findById(id);
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static TodoItem getItem(final String label){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                res = Database.getDb().todoItemDao().findByName(label);
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void removeItem(final TodoItem item){
        try{
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Database.getDb().todoItemDao().delete(item);
                }
            });
            t.start();
            t.join();
        }catch (Exception ignored){

        }
    }

    public static void swap(final int id1, final int id2){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                TodoItem item1 = getItem(id1), item2 = getItem(id2);
                item1.setId(-1);
                getDb().todoItemDao().updateItem(item1);
                item2.setId(id1);
                getDb().todoItemDao().updateItem(item2);
                item1.setId(id2);
                getDb().todoItemDao().updateItem(item1);
            }
        });
        try{
            t.start();
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
