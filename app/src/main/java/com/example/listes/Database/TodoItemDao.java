package com.example.listes.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.listes.Items.TodoItem;

import java.util.List;

@Dao
public interface TodoItemDao {

    @Query("SELECT * FROM TodoItem ORDER BY id ASC")
    List<TodoItem> getAll();

    @Query("SELECT * FROM TodoItem WHERE label LIKE :label LIMIT 1")
    TodoItem findByName(String label);

    @Query("SELECT * FROM TodoItem WHERE id LIKE :id LIMIT 1")
    TodoItem findById(int id);

    @Update
    void updateItem(TodoItem item);

    @Insert
    void insertAll(TodoItem... items);

    @Delete
    void delete(TodoItem item);

    @Query("DELETE FROM TodoItem")
    void deleteAll();
}