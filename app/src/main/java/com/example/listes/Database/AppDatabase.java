package com.example.listes.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.listes.Items.TodoItem;


@Database(entities = {TodoItem.class}, version = 5, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TodoItemDao todoItemDao();
}

