package com.example.listes.Items;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.listes.Activity.CreateItemActivity;
import com.example.listes.Activity.MainActivity;

public class TodoItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final RecyclerAdapter mAdapter;
    private final MainActivity activity;

    public TodoItemTouchHelperCallback(RecyclerAdapter mAdapter, MainActivity activity) {
        this.mAdapter = mAdapter;
        this.activity = activity;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    final static int DROITE = 32;
    final static int GAUCHE = 16;
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if(direction == DROITE){
            mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
        }
        else if(direction == GAUCHE){
            Intent i = new Intent(activity.getApplicationContext() , CreateItemActivity.class) ;
            TodoItem item = ((RecyclerAdapter.ItemHolder)viewHolder).getItem();
            i.putExtra("item", item);
            activity.startActivity(i);
        }
    }




}
