package com.example.listes.Items;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.listes.Database.Database;
import com.example.listes.R;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ItemHolder> {
    private List<TodoItem> items;
    private static Typeface typeface;

    public RecyclerAdapter(List<TodoItem> items, Typeface t) {
        this.items = items;
        typeface = t;
        System.out.println(t.isBold());
    }


    public List<TodoItem> getItems() {
        return items;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View inflatedView = LayoutInflater. from ( viewGroup.getContext () ).inflate(R.layout.row ,
                viewGroup, false );
        return new ItemHolder ( inflatedView ) ;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder itemHolder, int i) {
        TodoItem item = this.items.get(i);
        itemHolder.bind(item);
    }

    public void onItemDismiss(int pos){
        Database.removeItem(items.get(pos  ));
        refresh();
    }

    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(items, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(items, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        Database.swap(fromPosition+1, toPosition+1
        );
        return true;
    }



    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public void refresh(){
        this.getItems().clear();
        this.getItems().addAll(Database.loadDataSet());
        this.notifyDataSetChanged();
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        TextView nameView;
        ImageView image;
        Switch switch1;
        TextView dateEcheance;
        TodoItem item;


        public ItemHolder ( View itemView ) {
            super ( itemView ) ;
            nameView = itemView.findViewById(R.id.itemname);
            image = itemView.findViewById(R.id.tagColor);
            switch1 = itemView.findViewById(R.id.switch1);
            dateEcheance = itemView.findViewById(R.id.echeance);
        }

        public TodoItem getItem(){
            return item;
        }

        public void bind(final TodoItem item){
            this.item = item;
            this.nameView.setTypeface(typeface, Typeface.BOLD);
            this.nameView.setText(item.getLabel());
            this.image.setBackgroundColor(item.getTag().getColor());

            this.dateEcheance.setText(new SimpleDateFormat("dd-MM-yy").format(new Date(item.getEcheance())));


            this.switch1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setDone(switch1.isChecked());
                    if(item.isDone()) {
                        itemView.setBackgroundColor(Color.GRAY);
                        nameView.setTextColor(Color.WHITE);
                        dateEcheance.setTextColor(Color.WHITE);
                    }
                    else{
                        itemView.setBackgroundColor(Color.WHITE);
                        nameView.setTextColor(Color.GRAY);
                        dateEcheance.setTextColor(Color.GRAY);
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Database.getDb().todoItemDao().updateItem(item);
                        }
                    }).start();
                }
            });

            if(item.isDone()){
               switch1.performClick();
            }

        }

        boolean res;


        @Override
        public boolean onLongClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
            builder.setCancelable(true);
            builder.setTitle("Suppression");
            builder.setMessage("Vous êtes sûr de sur de supprimer cet item ?");
            builder.setPositiveButton("Oui",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            res = true;
                            Thread t = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Database.getDb().todoItemDao().delete(item);
                                }
                            });
                            t.start();
                            try{
                                t.join();
                            }catch (Exception e) {}

                            RecyclerAdapter.this.getItems().removeAll(RecyclerAdapter.this.getItems());
                            for(TodoItem item : Database.loadDataSet())
                                RecyclerAdapter.this.getItems().add(item);

                            RecyclerAdapter.this.notifyDataSetChanged();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    res = false;
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return res;
        }

    }
}
