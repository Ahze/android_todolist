package com.example.listes.Items;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Color;
import android.support.annotation.NonNull;

import com.example.listes.R;

import java.io.Serializable;

/**
 * Created by phil on 06/02/17.
 */
@Entity
public class TodoItem implements Serializable {


    public enum Tags {
        Faible(R.id.faibleBouton, Color.GREEN), Normal(R.id.moyenBouton,Color.YELLOW), Important(R.id.importantBouton, Color.RED);

        private int color, id;
        Tags(int id, int color) {
            this.id = id;
            this.color = color;
        }

        public int getColor() {
            return color;
        }
        public int getId() { return id;}
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "label")
    private String label;

    @ColumnInfo(name = "tag")
    private int tagNum;

    @ColumnInfo(name = "done")
    private boolean done;

    @ColumnInfo(name = "dateEcheance")
    private long echeance;

    @Ignore
    public TodoItem(String label, Tags t, long date) {
        this(label, t.ordinal(), false, date);
    }

    public TodoItem(@NonNull String label, int tagNum, boolean done, long echeance) {
        this.label = label;
        this.tagNum = tagNum;
        this.done = done;
        this.echeance = echeance;
    }

    public String getLabel() { return label; }

    public long getEcheance() { return echeance; }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Tags getTag(){
        return Tags.values()[this.tagNum];
    }

    public int getTagNum() {
        return tagNum;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public void setTag(Tags tag) {
        this.tagNum = tag.ordinal();
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setTagNum(int tagNum) {
        this.tagNum = tagNum;
    }

    public void setEcheance(long echeance) {
        this.echeance = echeance;
    }
}
